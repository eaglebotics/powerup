package org.usfirst.frc.team5410.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/*
 * A class to rotate a single motor a certain number of times.
 */

public class RotateMotor extends Command
{
	private WPI_TalonSRX motorController;
	private int rotateNum;
	private final int TICKS_PER_ROTATION = 80;

    public RotateMotor(WPI_TalonSRX motor, int rotations) {
    	super("RotateMotor"); // Tells Command class that there's a new sheriff in town
    	
    	motorController = motor;
    	motorController.setSelectedSensorPosition(0, 0, 10000); // Reset encoder position to 0 (not actual position)
    	
    	rotateNum = rotations;
		
    }

    // 	initialize() - This method sets up the command and is called immediately before the command is executed for the first time and every subsequent time it is started .
    //  Any initialization code should be here. 
    protected void initialize() {
    }

    /*
     *	execute() - This method is called periodically (about every 20ms) and does the work of the command. Sometimes, if there is a position a
     *  subsystem is moving to, the command might set the target position for the subsystem in initialize() and have an empty execute() method.
     */
    protected void execute() {
		motorController.set(0.1); // set() starts motor at a certain speed
		SmartDashboard.putNumber("Encoder#", motorController.getSelectedSensorPosition(0));
		
    	if (motorController.getSelectedSensorPosition(0) >= (TICKS_PER_ROTATION * rotateNum)) {
    		motorController.set(0); // Stop the motor
    	}
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
    	if (Robot.timer.get() >= 15) { // after 15 seconds, Auto is over
    		Robot.timer.stop();
    		Robot.timer.reset();
    		return true;
    	}
    	return false;
    }

}
