package org.usfirst.frc.team5410.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.networktables.NetworkTable;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import com.ctre.phoenix.motorcontrol.*;
public class basicAuto extends Command {
	WPI_TalonSRX motor1 = null;
	WPI_TalonSRX motor2 = null;
	WPI_TalonSRX motor3 = null;
	WPI_TalonSRX motor4 = null;
	int ticks = 0;
	boolean hasRun = false;
	public basicAuto(WPI_TalonSRX motor1c, WPI_TalonSRX motor2c, WPI_TalonSRX motor3c, WPI_TalonSRX motor4c, int ticksc) {
		WPI_TalonSRX motor1 = motor1c;
		WPI_TalonSRX motor2 = motor2c;
		WPI_TalonSRX motor3 = motor3c;
		WPI_TalonSRX motor4 = motor4c;
		ticks = ticksc;
	}
	
	public void rotateMotorEncoder(WPI_TalonSRX motor, int ticks)
	{
		motor.set(0.1); // set() starts motor at a certain speed
		SmartDashboard.putNumber("Encoder#", motor.getSelectedSensorPosition(0));
		
    	if (motor.getSelectedSensorPosition(0) >= (ticks)) {
    		motor.set(0); // Stop the motor
    	}
	}
	public void moveRobotTicks(int ticks)
	{
		rotateMotorEncoder(motor1, ticks);
		rotateMotorEncoder(motor2, ticks);
		rotateMotorEncoder(motor3, ticks);
		rotateMotorEncoder(motor4, ticks);
		hasRun = true;
	}
    // 	initialize() - This method sets up the command and is called immediately before the command is executed for the first time and every subsequent time it is started .
    //  Any initialization code should be here. 
    protected void initialize() {
    	
    }

    /*
     *	execute() - This method is called periodically (about every 20ms) and does the work of the command. Sometimes, if there is a position a
     *  subsystem is moving to, the command might set the target position for the subsystem in initialize() and have an empty execute() method.
     */
    protected void execute() {
    	while(!isFinished() && !hasRun)
    	{
    		moveRobotTicks(ticks);
    	}
    }


	@Override	
	protected boolean isFinished() {

    	if (Robot.timer.get() >= 15) { // after 15 seconds, Auto is over
    		Robot.timer.stop();
    		Robot.timer.reset();
    		return true;
    	}
    	return false;
	}

}
