package org.usfirst.frc.team5410.robot;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.networktables.NetworkTable;

public class CheckForBlock extends Command {

	public CheckForBlock() {
		super("CheckForBlock");
	}
	
    // 	initialize() - This method sets up the command and is called immediately before the command is executed for the first time and every subsequent time it is started .
    //  Any initialization code should be here. 
    protected void initialize() {
    	
    }

    /*
     *	execute() - This method is called periodically (about every 20ms) and does the work of the command. Sometimes, if there is a position a
     *  subsystem is moving to, the command might set the target position for the subsystem in initialize() and have an empty execute() method.
     */
    protected void execute() {
    	System.out.println(Limelight.getPos());
    }


	@Override	
	protected boolean isFinished() {

    	if (Robot.timer.get() >= 15) { // after 15 seconds, Auto is over
    		Robot.timer.stop();
    		Robot.timer.reset();
    		return true;
    	}
    	return false;
	}

}
