package org.usfirst.frc.team5410.robot;

import edu.wpi.first.wpilibj.networktables.NetworkTable;

public class Limelight {
	public static NetworkTable table = NetworkTable.getTable("limelight");
	
	public static String getPos() {
    	double targetOffsetAngle_Horizontal = table.getNumber("tx", 0); // Self-explanatory
    	double targetOffsetAngle_Vertical = table.getNumber("ty", 0);
    	double targetArea = table.getNumber("ta", 0);
    	double targetSkew = table.getNumber("ts", 0);
    	
    	return "targetArea: " + targetArea + " targetSkew: " + targetSkew; // TODO 
	}
	
}
