package org.usfirst.frc.team5410.robot;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.AnalogPotentiometer;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DigitalOutput;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
//import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Sendable;
import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.interfaces.Potentiometer;
//import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

//import java.util.concurrent.TimeUnit;

import com.ctre.phoenix.motorcontrol.*;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;
/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {

	final String defaultAuto = "Testing (Default) Auto"; // The name of the default auto mode
	final String customAuto = "Game Auto"; // Main auto mode
	final String blockAuto = "Block Auto";
	final String BasicAuto = "Basic Auto";
	double error = 0;
	int counter = 0;
	String motorSpeed;
	String autoSelected;
	SendableChooser autoChooser;
	Command autonomousCommand;
	boolean teleOpInit = false;
	boolean autoRun = false;
	AHRS navx = new AHRS(SerialPort.Port.kMXP);
	String gameData = null;
	boolean s1 = false;
	boolean s2,s3,s4,s5,s6 = false;
	
	boolean t1 = false;
	//DigitalOutput pin0 = new DigitalOutput(0);
	//DigitalOutput pin1 = new DigitalOutput(1);
	//DigitalOutput pin2 = new DigitalOutput(2);
	//DigitalOutput pin9 = new DigitalOutput(9);
	DigitalInput pin0 = new DigitalInput(0);
	DigitalInput pin1 = new DigitalInput(1);
	//DigitalOutput pin2 = new DigitalOutput(2);
	//DigitalOutput pin9 = new DigitalOutput(9);
	//CANTalon motor1 = new CANTalon(0);
	//1-4 drive, 5-6 suck, 7-9 lift, 10 arm
	WPI_TalonSRX motor1 = new WPI_TalonSRX(1);
	WPI_TalonSRX motor2 = new WPI_TalonSRX(2);
	WPI_TalonSRX motor3 = new WPI_TalonSRX(3);
	WPI_TalonSRX motor4 = new WPI_TalonSRX(4);
	WPI_TalonSRX motor5 = new WPI_TalonSRX(5);
	WPI_TalonSRX motor6 = new WPI_TalonSRX(6);
	WPI_TalonSRX motor7 = new WPI_TalonSRX(7);
	WPI_TalonSRX motor8 = new WPI_TalonSRX(8);
	WPI_TalonSRX motor9 = new WPI_TalonSRX(9);
	WPI_TalonSRX motor10 = new WPI_TalonSRX(10);
	
	
	Servo servo1 = new Servo(0);
	Servo servo2 = new Servo(9);
	DifferentialDrive drive = new DifferentialDrive(motor1,motor3);

	Joystick joystick1 = new Joystick(0);
	Joystick joystick2 = new Joystick(1);

	
	AnalogInput ai = new AnalogInput(0);
	//Potentiometer pot = new AnalogPotentiometer(ai, 360, 30);
	Potentiometer pot = null;
	//RobotDrive drive = new RobotDrive(motor1, motor2, motor3, motor4);
	
	
	DriveTrain driveTrain = new DriveTrain(motor1, motor2, motor3, motor4, motor5, motor6, motor7, motor8, motor9, motor10, navx, pin0, pin1, pot);
	
	
	//Armbb arm = new Armbb(motor5, motor6, motor7, motor8, motor9, motor10);
	
	
	BasicAuto basicAuto =  new BasicAuto(driveTrain, 2500, navx);
	LeftSideSwitch leftSideSwitch = new LeftSideSwitch(driveTrain, navx);
	MiddleLineAuto middleLineAuto = new MiddleLineAuto(driveTrain, navx);
	MiddleSwitchAuto middleSwitchAuto = new MiddleSwitchAuto(driveTrain, navx);
	RightSideSwitch rightSideSwitch = new RightSideSwitch(driveTrain, navx);
	SideCrossLine sideCrossLine = new SideCrossLine(driveTrain, navx);
	ArmTest armTest = new ArmTest(driveTrain, navx);
	public static Timer timer = new Timer(); // Keep track of time

	//Encoder enc = new Encoder(0, 1, false, Encoder.EncodingType.k4X); // Not needed; WPI_TalonSRX has everything we need
	
	
	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() {
        CameraServer.getInstance().startAutomaticCapture();
    	motor3.setSelectedSensorPosition(0, 0, 10000);
		motor1.setSelectedSensorPosition(0, 0, 10000);
		
		autoChooser = new SendableChooser();
		autoChooser.addObject("Test", basicAuto);
		autoChooser.addObject("Left Side Switch", leftSideSwitch);
		autoChooser.addObject("middleLineAuto", middleLineAuto);
		autoChooser.addObject("middleSwitchAuto", middleSwitchAuto);
		autoChooser.addObject("rightSideSwitch", rightSideSwitch);
		autoChooser.addObject("sideCrossLine", sideCrossLine);
		
		SmartDashboard.putString("game data",DriverStation.getInstance().getGameSpecificMessage());
		SmartDashboard.putBoolean("pin0", pin0.get());
		SmartDashboard.putBoolean("pin1", pin1.get());
		motor1.configSelectedFeedbackSensor(com.ctre.phoenix.motorcontrol.FeedbackDevice.QuadEncoder, 0, 0);
		motor1.setSensorPhase(false);
		motor3.configSelectedFeedbackSensor(com.ctre.phoenix.motorcontrol.FeedbackDevice.QuadEncoder, 0, 0);
		motor3.setSensorPhase(false);
		drive.setSafetyEnabled(false);
		motor1.setInverted(true);
		motor2.setInverted(true);
		motor2.follow(motor1);
		//motor4.follow(motor3);
		motor4.follow(motor3);
		servo1.set(.02);
		//motor8.follow(motor7);
		//motor9.follow(motor7);
		
		SmartDashboard.putNumber("AutoChooser42", 9.0);
	}

	/**
	 * This autonomous (along with the chooser code above) shows how to select
	 * between different autonomous modes using the dashboard. The sendable
	 * chooser code works with the Java SmartDashboard. If you prefer the
	 * LabVIEW Dashboard, remove all of the chooser code and uncomment the
	 * getString line to get the auto name from the text box below the Gyro
	 *
	 * You can add additional auto modes by adding additional comparisons to the
	 * switch structure below with additional strings. If using the
	 * SendableChooser make sure to add them to the chooser code above as well.
	 */
	@Override
	public void autonomousInit() { // Called as soon as Auto mode starts
		/*String gameData = DriverStation.getInstance().getGameSpecificMessage();
		SmartDashboard.putString("GameData:", gameData);
		autonomousCommand = (Command)autoChooser.getSelected();
		autonomousCommand.start();
		// autoSelected = SmartDashboard.getString("Auto Selector",
		// defaultAuto);
		System.out.println("Auto selected: " + autoSelected);
		
		timer.reset();
		timer.start();*/
		
		//basicAuto.initialize();
		motor3.setSelectedSensorPosition(0, 0, 10000);
		motor1.setSelectedSensorPosition(0, 0, 10000);
	
		/*if(autoChooser.getName() == leftSideSwitch.getName())
		{
			leftSideSwitch.initialize();
		}
		else if(autoChooser.getName() == middleLineAuto.getName())
		{
			middleLineAuto.initialize();
		}
		else if(autoChooser.getName() == middleSwitchAuto.getName())
		{
			middleSwitchAuto.initialize();
		}
		else if(SmartDashboard.getNumber("num", 42.0) == 9.0)
		{
			rightSideSwitch.initialize();
		}
		else if(autoChooser.getName() == rightSideSwitch.getName())
		{
			rightSideSwitch.initialize();
		}
		else if(autoChooser.getName() == sideCrossLine.getName())
		{
			sideCrossLine.initialize();
		}
		else
		{
			sideCrossLine.initialize();
		}*/
		servo1.set(.02);
		//basicAuto.initialize();
		/*
		if(SmartDashboard.getNumber("AutoChooser42", 42.0) == 9.0)
		{
			sideCrossLine.initialize();
		}
		else if(SmartDashboard.getNumber("AutoChooser42", 42.0) == 1.0)
		{
			rightSideSwitch.initialize();
		}
		else if(SmartDashboard.getNumber("AutoChooser42", 42.0) == 2.0)
		{
			leftSideSwitch.initialize();
		}*/
		timer.reset();
		timer.start();
		 gameData = DriverStation.getInstance().getGameSpecificMessage();
	}

	/**
	 * This function is called periodically during autonomous (~ every 20 miliseconds)
	 */
	@Override
	public void autonomousPeriodic() {
		//Scheduler.getInstance().run();
		//motor1.getSelectedSensorPosition(0);
		SmartDashboard.putNumber("num2", SmartDashboard.getNumber("num", 42.0));
		servo1.set(.02);
		SmartDashboard.putNumber("motor#1 velocity", motor1.getSelectedSensorVelocity(0));
		SmartDashboard.putNumber("motor#3 velocity", motor3.getSelectedSensorVelocity(0));
		SmartDashboard.putNumber("diff", Math.abs(motor1.getSelectedSensorVelocity(0))-Math.abs(motor3.getSelectedSensorVelocity(0)));
		SmartDashboard.putNumber("Navx", navx.getAngle());
		SmartDashboard.putNumber("Encoder#3", motor3.getSelectedSensorPosition(0));
		counter = counter + 1;
		SmartDashboard.putNumber("counter", counter);
		
		
		/*if(autoChooser.getName() == "Left Side Switch")
		{
			leftSideSwitch.execute();
		}
		else if(autoChooser.getName() == middleLineAuto.getName())
		{
			middleLineAuto.execute();
		}
		else if(autoChooser.getName() == middleSwitchAuto.getName())
		{
			middleSwitchAuto.execute();
		}
		else if(SmartDashboard.getNumber("num", 42.0) == 9.0)
		{
			rightSideSwitch.execute();
		}
		else if(autoChooser.getName() == leftSideSwitch.getName())
		{
			leftSideSwitch.execute();
		}
		else if(autoChooser.getName() == sideCrossLine.getName())
		{
			sideCrossLine.execute();
		}
		else
		{
			rightSideSwitch.execute();
		}*/
		
		if(SmartDashboard.getNumber("AutoChooser42", 42.0) == 9.0)
		{
			driveTrain.moveStraight(.3,9800);
		}
		else if(SmartDashboard.getNumber("AutoChooser42", 42.0) == 1.0)
		{
			if(s1 == false)
			{
			s1 = driveTrain.moveStraight(.3,9800);
			}
			else if(s2 == false)
			{
				s2 = driveTrain.turnLeft(70);
			}
			if(t1 = false)
			{
				timer.reset();
				timer.start();
				t1 = true;
			}
			if(timer.get()>11)
			{
				motor5.set(0);
				motor6.set(0);
			}
			else if(timer.get()>10)
			{
				if(gameData.equals("RRR")||gameData.equals("RRL")||gameData.equals(gameData == "RLR")||gameData.equals("RLL"))
				{
				motor5.set(.3);
				motor6.set(.3);
				}
				else
				{
					motor5.set(0);
					motor6.set(0);
				}
			}
			
			else if(timer.get()>3)
				{
			
					motor10.set(0);
					t1 = true;
				}
				
				else
				{
					motor10.set(.6);
				}
			}
		else if(SmartDashboard.getNumber("AutoChooser42", 42.0) == 2.0)
		{
			if(s1 == false)
			{
				s1 = driveTrain.moveStraight(.3,9800);
			}
			else if(s2 == false)
			{
				s2 = driveTrain.turnRight(70);
			}
			if(t1 = false)
			{
				timer.reset();
				timer.start();
				t1 = true;
			}
			if(timer.get()>11)
			{
				motor5.set(0);
				motor6.set(0);
			}
			else if(timer.get()>10)
			{
				if(gameData.equals("LLL")||gameData.equals("LRR")||gameData.equals(gameData == "LRL")||gameData.equals("LLR"))
				{
				motor5.set(.3);
				motor6.set(.3);
				}
				else
				{
					motor5.set(0);
					motor6.set(0);
				}
			}
			
			else if(timer.get()>3)
				{
			
					motor10.set(0);
					t1 = true;
				}
				
				else
				{
					motor10.set(.6);
				}
			}
		else if(SmartDashboard.getNumber("AutoChooser42", 42.0) == 3.0)
		{
			if(gameData.equals("RRR")||gameData.equals("RRL")||gameData.equals(gameData == "RLR")||gameData.equals("RLL"))
			{
				if(s1 == false)
				{
				s1 = driveTrain.moveStraight(.3,9800);
				}
				else if(s2 == false)
				{
					s2 = driveTrain.turnRight(290);
				}
				if(t1 = false)
				{
					timer.reset();
					timer.start();
					t1 = true;
				}
				if(timer.get()>11)
				{
					motor5.set(0);
					motor6.set(0);
				}
				else if(timer.get()>10)
				{
					if(gameData.equals("RRR")||gameData.equals("RRL")||gameData.equals(gameData == "RLR")||gameData.equals("RLL"))
					{
					motor5.set(.3);
					motor6.set(.3);
					}
					else
					{
						motor5.set(0);
						motor6.set(0);
					}
				}
				
				else if(timer.get()>3)
					{
				
						motor10.set(0);
						t1 = true;
					}
					
					else
					{
						motor10.set(.6);
					}
				}
			else
			{
				if(t1 == false)
				{
					motor10.set(0.6);
					timer.reset();
					timer.start();
					t1 = true;
				}
				if(timer.get()>3)
					motor10.set(0);
				if(s1 == false)
				{
				s1 = driveTrain.moveStraight(.5,11000);
				}
				else if(s2 == false)
					s2 = driveTrain.turnLeft(70);
				else if(s3 == false)
					s3 = driveTrain.moveStraight(.5,6000);
				else if(s4 == false)
					s4 = driveTrain.turnLeft(70);
				else if(s5 == false)
				{
					motor5.set(.3);
					motor6.set(.3);
					s5 = true;
				}
				else if(s6 == false && timer.get()>13)
				{
					motor5.set(0);
					motor6.set(0);
					s6 = true;
				}
			}
		}
		else if(SmartDashboard.getNumber("AutoChooser42", 42.0) == 4.0)
		{
			if(gameData.equals("LLL")||gameData.equals("LRR")||gameData.equals("LRL")||gameData.equals("LLR"))
			{
				if(s1 == false)
				{
				s1 = driveTrain.moveStraight(.3,9800);
				}
				else if(s2 == false)
				{
					s2 = driveTrain.turnRight(70);
				}
				if(t1 = false)
				{
					timer.reset();
					timer.start();
					t1 = true;
				}
				if(timer.get()>11)
				{
					motor5.set(0);
					motor6.set(0);
				}
				else if(timer.get()>10)
				{
					if(gameData.equals("LLL")||gameData.equals("LRR")||gameData.equals("LRL")||gameData.equals("LLR"))
					{
					motor5.set(.3);
					motor6.set(.3);
					}
					else
					{
						motor5.set(0);
						motor6.set(0);
					}
				}
				
				else if(timer.get()>3)
					{
				
						motor10.set(0);
						t1 = true;
					}
					
					else
					{
						motor10.set(.6);
					}
				}
			else
			{
				if(t1 == false)
				{
					motor10.set(0.6);
					timer.reset();
					timer.start();
					t1 = true;
				}
				if(timer.get()>3)
					motor10.set(0);
				if(s1 == false)
				{
				s1 = driveTrain.moveStraight(.5,11000);
				}
				else if(s2 == false)
					s2 = driveTrain.turnRight(70);
				else if(s3 == false)
					s3 = driveTrain.moveStraight(.5,6000);
				else if(s4 == false)
					s4 = driveTrain.turnRight(70);
				else if(s5 == false)
				{
					motor5.set(.3);
					motor6.set(.3);
					s5 = true;
				}
				else if(s6 == false && timer.get()>13)
				{
					motor5.set(0);
					motor6.set(0);
					s6 = true;
				}
			}
		}
		else if(SmartDashboard.getNumber("AutoChooser42", 42.0) == 5.0)
		{
			if(t1 = false)
			{
				motor10.set(0.6);
				timer.reset();
				timer.start();
				t1 = true;
			}
			if(timer.get()>3)
				motor10.set(0);
			if(s1 == false)
				s1 = driveTrain.moveStraight(.5,4000);
			else if(s2 == false)
			{
				if(gameData.equals("RRR")||gameData.equals("RLR"))
				{
					s2 = driveTrain.turnRight(45);
				}
				else
				{
					s2 = driveTrain.turnLeft(45);
				}
			}
			else if(s3 == false)
				s3 = driveTrain.moveStraight(.5, 2000);
			else if(s4 == false)
			{
				if(gameData.equals("RRR")||gameData.equals("RLR"))
				{
					s4 = driveTrain.turnLeft(45);
				}
				else
				{
					s4 = driveTrain.turnRight(45);
				}
			}
			else if(s5 == false)
			{
				motor5.set(.3);
				motor6.set(.3);
				s5 = true;
			}
			else if(s6 == false && timer.get()>13)
			{
				motor5.set(0);
				motor6.set(0);
			}
		}
		//sideCrossLine.execute();
	
		//basicAuto.execute();
		//driveTrain.moveStraight(0.5, 899898);
		
			//moveStraight(0.5);
			
		//SmartDashboard.putString("autochooser", autoChooser.getName());
		//basicAuto.execute();
	}
	
	//SerialPort port = new SerialPort(0, null);
	public boolean moveStraight(double speed, int ticks) {
	
		if(motor3.getSelectedSensorPosition(0) < (ticks))
		{
		error = error + (motor1.getSelectedSensorVelocity(0) - motor3.getSelectedSensorVelocity(0))*0.0004; 
		
		if (Math.abs(speed) > 0.01) {
				//error =+ (motor1.getSelectedSensorVelocity(0) - motor3.getSelectedSensorVelocity(0))/150; 
			SmartDashboard.putNumber("speed12", speed-error);
			if (speed == 0) {
			
				
			}
			SmartDashboard.putNumber("speed34", speed);

				motor1.set(speed-error);
				motor2.set(speed-error);

				motor3.set(speed);
				motor4.set(speed);

				

				return false;
		}
		return false;
		
		/*else if (speed < 0)
		{
				motor1.set(speed-error);
				motor3.set(speed);	
		}*/
		
		
		}
		else
		{
			motor1.set(0);
			motor2.set(0);
			motor3.set(0);
			motor4.set(0);
			return true;
			
		}
		
		}
	/**
	 * This function is called periodically during operator control
	 */

	@SuppressWarnings("deprecation")
	@Override
	public void teleopInit()
	{
		motor1.set(0);
		motor2.set(0);
		motor3.set(0);
		motor4.set(0);
		//motor5.set(0);
		//motor6.set(0);
		//motor7.set(0);
		//motor8.set(0);
		//motor9.set(0);
		//motor10.set(0);
		
	}
	public void teleopPeriodic() {
		//documentation
		//joystick 1 left joystick controls forward and backward(The Y axis(1))
		//Joystick 1 right joystick controls turning(The X axis(4))
		//joystick 1 right trigger controls push of cube grabber(axis 3)
		//joystick 1 left trigger controls suck of cube grabber(axis 2)
		//joystick 1 right bumper shoots at certain speed(toggleable(5))
		//joystick 1 left bumper shoots at certain speed(toggleable(4))
		//joystick 2 left joystick controls arm up and down(The Y axis(1))
		//joystick 2 right joystick controls lift up and down(The Y axis(5))
		
		
		if(teleOpInit == false)
		{
			motor1.set(0);
			motor2.set(0);
			motor3.set(0);
			motor4.set(0);
			motor5.set(0);
			motor6.set(0);
			motor7.set(0);
			motor8.set(0);
			motor9.set(0);
			motor10.set(0);
			teleOpInit = true;
		}
		
		
		//SmartDashboard.putNumber("Navx", navx.getAngle());
		//SmartDashboard.putNumber("Pot", pot.get());
		SmartDashboard.putNumber("AI", 4);
		/*joystick1.setRumble(RumbleType.kRightRumble, 1);
		motor1.set(-joystick1.getRawAxis(1));
		motor2.set(-joystick1.getRawAxis(1));
		motor3.set(joystick1.getRawAxis(5));
		motor4.set(joystick1.getRawAxis(5));
		tank /\ */
		if (Math.abs(joystick1.getRawAxis(1)) > 0.1)
		{
			//moveStraight(joystick1.getRawAxis(1));
			//drive.arcadeDrive(-joystick1.getRawAxis(1),0);
			//motor3.set(joystick1.getRawAxis(1));
			//motor1.set(joystick1.getRawAxis(1));
			motor3.set(-joystick1.getRawAxis(1));
			motor4.set(-joystick1.getRawAxis(1));

		}
		else
		{
			
			motor3.set(0);
			motor4.set(0);
		}
		if (Math.abs(joystick1.getRawAxis(5)) > 0.1)
		{
			motor1.set(-joystick1.getRawAxis(5));
			motor2.set(-joystick1.getRawAxis(5));
		}
		else
		{
			
			motor1.set(0);
			motor2.set(0);
		}
		if(joystick2.getRawAxis(1)>0.01)
		{
			motor10.set(-joystick2.getRawAxis(1)*.5);
		}
		else
		{
			motor10.set(-joystick2.getRawAxis(1));
		}
		/*
		else if(joystick2.getRawAxis(1)>0.01 && pot.get() > 332)
		{
			motor10.set(-joystick2.getRawAxis(1)*.4);
		}
		else if(pot.get() > 333)
		{
			motor10.set(1);
			
		}
		else
		{
			motor10.set(.25);
		}
		*/
		if(joystick1.getRawAxis(3)>0.01||joystick2.getRawAxis(3)>0.01)
		{
			motor5.set(joystick1.getRawAxis(3)+joystick2.getRawAxis(3));
			motor6.set(joystick1.getRawAxis(3)+joystick2.getRawAxis(3));
		}
		else if(joystick1.getRawAxis(2)>0.01||joystick2.getRawAxis(2)>0.01)
		{
			motor5.set(-joystick1.getRawAxis(2)-joystick2.getRawAxis(2));
			motor6.set(-joystick1.getRawAxis(2)-joystick2.getRawAxis(2));
		}
		else if(joystick1.getRawButton(5)||joystick2.getRawButton(5))
		{
			motor5.set(-.45);
			motor6.set(-.45);
		}
		else if(joystick1.getRawButton(6))
		{
			motor5.set(.45);
			motor6.set(.45);
		}
		else
		{
			motor5.set(0);
			motor6.set(0);
		}
		pin0.get(); // true = 1 (current) false = 0 (no current)
		//motor7.set(joystick1.getRawAxis(1));
		
		/*
		if (-joystick2.getRawAxis(5) < 0.01 && !pin1.get())
			{
			motor7.set(-joystick2.getRawAxis(5));
			motor8.set(-joystick2.getRawAxis(5));
			
			motor9.set(-joystick2.getRawAxis(5));
			
			}
		else if (-joystick2.getRawAxis(5) > 0.01 && !pin0.get())
			{
			motor7.set(-joystick2.getRawAxis(5));
			motor8.set(-joystick2.getRawAxis(5));
			
			
			motor9.set(-joystick2.getRawAxis(5));
			}
		else 
		{
			motor7.set(0);
			motor8.set(0);
			
			
			motor9.set(0);
		}
		*/
		
		SmartDashboard.putBoolean("pin0", pin0.get());
		SmartDashboard.putBoolean("pin1", pin1.get());
		//motor1.set(joystick1.getRawAxis(1));
		
		SmartDashboard.putNumber("Error Difference1",motor1.getSelectedSensorVelocity(0));
		SmartDashboard.putNumber("Error Difference3",motor3.getSelectedSensorVelocity(0));
		SmartDashboard.putNumber("Error Difference5",error);
		//driveTrain.driveForTicks(1000, joystick1.getRawAxis(1));
		if(joystick1.getRawButton(2)&&joystick2.getRawButton(2))
		{
		//	drive.tankDrive(-1, -1);
			//setPins(true, true, false);
			//pin9.set(true);
			servo1.set(1);
			
		}
		else
		{
			//pin9.set(false);
			servo1.set(.02);
			
		}
		
	
		SmartDashboard.putNumber("analog", ai.getValue());
		//SmartDashboard.putNumber("pot", pot.get());
	
	/*	else if(joystick1.getRawButton(4))
		{
			//setPins(true, true, true);
		}
			s
			
		else
	{

			setPins(false, false, false);
		
		//motor1.set(ControlMode.Velocity, joystick1.getRawAxis(1));
		//motor3.set(ControlMode.Velocity, -joystick1.getRawAxis(1));
		
		
					//drive.arcadeDrive(-joystick1.getRawAxis(1),-joystick1.getRawAxis(0)); // arcade
					//drive.arcadeDrive(0.2,0); // arcade

					//	motor1.set(joystick1.getRawAxis(1)*1);
			//motor3.set(joystick1.getRawAxis(1)*1);
		} 
*/
		
		
	}
	

	/**
	 * This function is called periodically during test mode
	 */
	public void moveStraight(double speed) {
		error = error + (motor1.getSelectedSensorVelocity(0) - motor3.getSelectedSensorVelocity(0))*0.0004; 

		if (Math.abs(speed) > 0.01) {
				//error =+ (motor1.getSelectedSensorVelocity(0) - motor3.getSelectedSensorVelocity(0))/150; 
				motor1.set(speed-error);
				motor3.set(speed);
		}
		
		/*else if (speed < 0)
		{
				motor1.set(speed-error);
				motor3.set(speed);	
		}*/
		else
		{
			motor1.set(speed);
			motor3.set(speed);	
		}
		//SmartDashboard.putNumber("Error Difference",error);
	}
	@Override
	public void testPeriodic() {
	}
	
	public void setPins(boolean a, boolean b, boolean c)
	{
		//pin0.set(a);
		//pin1.set(b);
		//pin2.set(c);
	}
}
