package org.usfirst.frc.team5410.robot;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class BasicAuto extends Command {
	DriveTrain driveTrain = null;
	
	boolean s1 = false;
	boolean t1 = false;
	boolean s2 = false;
	boolean t2 = false;
	boolean s3 = false;
    int ticks = 0;
    AHRS navx = null;
    String gameData = null;
    public static Timer timer = new Timer();
	public BasicAuto(DriveTrain driveTrainc, int d, AHRS navxc) 
	{
		driveTrain = driveTrainc;
		ticks = d;
		navx = navxc;
		
	}
	
    // 	initialize() - This method sets up the command and is called immediately before the command is executed for the first time and every subsequent time it is started .
    //  Any initialization code should be here. 
    protected void initialize() 
    {
    	navx.reset();
    	driveTrain.angle = navx.getAngle();
    	gameData = DriverStation.getInstance().getGameSpecificMessage();
    }

    /*
     *	execute() - This method is called periodically (about every 20ms) and does the work of the command. Sometimes, if there is a position a
     *  subsystem is moving to, the command might set the target position for the subsystem in initialize() and have an empty execute() method.
     */
    protected void execute() 
    {
    	SmartDashboard.putNumber("angleee", navx.getAngle());
    				
    			
    			if(s1 == false)
    			{
    				s1 = driveTrain.moveStraight(0.2, ticks);
    				timer.reset();
    				timer.start();
    		    	navx.reset();

    			}
    			else if (t1 == false)
    			{
        			if (timer.get() > 2)
        			{
        				t1 = true;
        			}
    			}
    						
    			else if(s2 == false)
    			{
    				SmartDashboard.putNumber("time", timer.get());
    				s2 = driveTrain.turnLeft(175);
    				driveTrain.resetEncoders();
    				timer.reset();
    				timer.start();
    			}
    			else if (!t2)
    			{
    				if (timer.get() > 2)
        			{
        				t2= true;
        				driveTrain.error = 0;
        				driveTrain.motor2.follow(driveTrain.motor1);
        				driveTrain.motor4.follow(driveTrain.motor3);

        			}
    			}
    			else if(s3 == false)
    			{	
    				s3 = driveTrain.moveStraight(0.2, ticks);
    			}
    			}
    			
    		
    	
    


	@Override
	protected boolean isFinished() {

    	if (Robot.timer.get() >= 15) { // after 15 seconds, Auto is over
    		Robot.timer.stop();
    		Robot.timer.reset();
    		return true;
    	}
    	return false;
	}

}
