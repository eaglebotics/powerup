package org.usfirst.frc.team5410.robot;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;


public class MiddleLineAuto extends Command{
	DriveTrain driveTrain = null;
	AHRS navx = null;
	
	String gameData = null;
	static Timer timer = new Timer();
	
	boolean s1 = false;
	boolean s2 = false;
	boolean s3 = false;
	boolean s4 = false;
	boolean s5 = false;
	boolean s6 = false;
	
	boolean b1 = false;
	public MiddleLineAuto(DriveTrain driveTrainc, AHRS navxc) 
	{
		driveTrain = driveTrainc;
		navx = navxc;
		
	}
	protected void initialize()
	{
		navx.reset();
    	driveTrain.angle = navx.getAngle();
    	gameData = DriverStation.getInstance().getGameSpecificMessage();
	}
	protected void execute()
	{
		
		if(s1 == false)
		{
			s1 = driveTrain.driveForInches(50, .3);
			timer.reset();
			timer.start();
	    	navx.reset();
		}
		else if(s2 == false)
		{
			if(gameData == "RRR")
			{
				s2 = driveTrain.turnLeft(90);
				timer.reset();
				timer.start();
		    	navx.reset();
			}
			else
			{
				s2 = driveTrain.turnRight(90);
				timer.reset();
				timer.start();
		    	navx.reset();
			}
		}
		else if(s3 == false)
		{
			s3 = driveTrain.driveForInches(60, .3);
			timer.reset();
			timer.start();
	    	navx.reset();
		}
		else if(s4 == false)
		{
			if(gameData == "RRL" || gameData == "RRR" || gameData == "RLL" || gameData == "RLR")
			{
				s4 = driveTrain.turnRight(90);
				timer.reset();
				timer.start();
		    	navx.reset();
			}
			else
			{
				s4 = driveTrain.turnLeft(90);
				timer.reset();
				timer.start();
		    	navx.reset();
			}
		}
		else if(s5 == false)
		{
			s5 = driveTrain.driveForInches(50, .3);
			timer.reset();
			timer.start();
	    	navx.reset();
		}
		
	}
	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}
}
