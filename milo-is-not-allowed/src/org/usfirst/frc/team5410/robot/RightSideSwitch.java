package org.usfirst.frc.team5410.robot;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;


public class RightSideSwitch extends Command{
	DriveTrain driveTrain = null;
	AHRS navx = null;
	
	String gameData = null;
	static Timer timer = new Timer();
	
	boolean s1 = false;
	boolean s2 = false;
	boolean s3 = false;
	boolean s4 = false;
	boolean s5 = false;
	boolean s6 = false;
	
	boolean b1 = false;
	
	boolean t1 = false;
	public RightSideSwitch(DriveTrain driveTrainc, AHRS navxc) 
	{
		driveTrain = driveTrainc;
		navx = navxc;
		
	}
	protected void initialize()
	{
		navx.reset();
    	driveTrain.angle = navx.getAngle();
    	gameData = DriverStation.getInstance().getGameSpecificMessage();
    	
    
	}
	protected void execute()
	{
		//gameData = DriverStation.getInstance().getGameSpecificMessage();
		if(gameData.equals("RRR")||gameData.equals("RRL")||gameData.equals(gameData == "RLR")||gameData.equals("RLL"))
		{
			if(b1 == false)
			{
				b1 = driveTrain.moveArmPot(-0.7, 260);
			}
			else if(s1 == false)
			{
				s1 = driveTrain.moveStraight( .3, 9500);
				timer.reset();
				timer.start();
		    	navx.reset();
			}
			else if (t1 == false)
			{
				if (timer.get() > 1)
				{
					t1 = true;
				}
			}
			else if(s2 == false)
			{
				timer.reset();
				timer.start();
				s2 = driveTrain.turnLeft(75);
				
				
			}
			else if(s3 == false)
			{
				if (timer.get() > 1)
				{
					t1 = true;
					driveTrain.pushCube(0);
				}
				driveTrain.motor5.set(-.2);
				driveTrain.motor6.set(-.2);
			}
			else if(s4 == false)
			{
				s4 =true;
			}
			else {
				driveTrain.motor5.set(0);
				driveTrain.motor6.set(0);
				driveTrain.driveForward(0);
			}
		}
		else
		{/*
			if(b1 == false)
				b1 = driveTrain.moveArmPot(-0.7, 260);
				*/
			if(s1 == false)
				s1 = driveTrain.moveStraight( .3, 9800);
			else
			{
				driveTrain.driveForward(0);
			}
			/*
			else if(s2 == false)
				s2 = driveTrain.turnLeft(90);
			
			else if(s3 == false)
				s3 = driveTrain.moveStraight( .3, 7000);
			else if(s4 == false)
			{
				s4 = driveTrain.turnLeft(90);
				timer.reset();
				timer.start();
			}
			else if(s5 == false)
			{
				if (timer.get() > 1)
				{
					s5 = true;
					driveTrain.pushCube(0);
				}
				driveTrain.pushCube(.2);
			}
	
		*/
	}
	}
	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}
}
