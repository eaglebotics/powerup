package org.usfirst.frc.team5410.robot;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


public class SideCrossLine extends Command{
	DriveTrain driveTrain = null;
	AHRS navx = null;
	
	String gameData = null;
	static Timer timer = new Timer();
	
	boolean s1 = false;
	boolean s2 = false;
	boolean s3 = false;
	boolean s4 = false;
	boolean s5 = false;
	boolean s6 = false;
	
	boolean b1 = false;
	
	boolean t1 = false;
	public SideCrossLine(DriveTrain driveTrainc, AHRS navxc) 
	{
		driveTrain = driveTrainc;
		navx = navxc;
		
	}
	protected void initialize()
	{
		navx.reset();
    	driveTrain.angle = navx.getAngle();
    	gameData = DriverStation.getInstance().getGameSpecificMessage();
	}
	protected void execute()
	{
		
		if(s1 == false)
		{
			SmartDashboard.putBoolean("s1", s1);
			s1 = driveTrain.moveStraight( .3, 9800);
			timer.reset();
			timer.start();
	    	navx.reset();
		}
		else
		{
			driveTrain.driveForward(0);
		}
		
		
		
		
	}
	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}
}
