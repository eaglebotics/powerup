package org.usfirst.frc.team5410.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.interfaces.Potentiometer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class DriveTrain {
	static int speedzero = 0;
	final int TICKS_PER_INCH = 74;
	WPI_TalonSRX motor1 = null;
	WPI_TalonSRX motor2 = null;
	WPI_TalonSRX motor3 = null;
	WPI_TalonSRX motor4 = null;
	WPI_TalonSRX motor5 = null;
	WPI_TalonSRX motor6 = null;
	WPI_TalonSRX motor7 = null;
	WPI_TalonSRX motor8 = null;
	WPI_TalonSRX motor9 = null;
	WPI_TalonSRX motor10 = null;
	DigitalInput pin0 = null;
	DigitalInput pin1 = null;
	AHRS navx = null;
	Potentiometer pot = null;
	//DigitalInput pin0 = new DigitalInput(0);
	//DigitalInput pin1 = new DigitalInput(1);
	double angle = 0;
	double error = 0;

	
	public DriveTrain(WPI_TalonSRX motor1c, WPI_TalonSRX motor2c, WPI_TalonSRX motor3c, WPI_TalonSRX motor4c, WPI_TalonSRX motor5c, WPI_TalonSRX motor6c,WPI_TalonSRX motor7c,WPI_TalonSRX motor8c,WPI_TalonSRX motor9c,WPI_TalonSRX motor10c, AHRS navxc, DigitalInput pin0c, DigitalInput pin1c, Potentiometer potc)
	{
		motor1 = motor1c;
		motor2 = motor2c;
		motor3 = motor3c;
		motor4 = motor4c;
		motor5 = motor5c;
		motor6 = motor6c;
		motor7 = motor7c;
		motor8 = motor8c;
		motor9 = motor9c;
		motor10 = motor10c;
		//motor1.setInverted(true);
		//motor2.setInverted(true);
		motor2.follow(motor1);
		motor4.follow(motor3);
		navx = navxc;
		pin0 = pin0c;
		pin1 = pin1c;
		pot = potc;
	}
	
	public DriveTrain(AHRS navxc)
	{
		//motor1.setInverted(true);
		//motor2.setInverted(true);
		navx = navxc;
	}
	public DriveTrain(WPI_TalonSRX motor1c, WPI_TalonSRX motor2c, WPI_TalonSRX motor3c, WPI_TalonSRX motor4c,  AHRS navxc)
	{
		motor1 = motor1c;
		motor2 = motor2c;
		motor3 = motor3c;
		motor4 = motor4c;

		//motor1.setInverted(true);
		//motor2.setInverted(true);
		navx = navxc;
	}
	public void driveLeftSide(double speed)
	{
		motor1.set(speed);
		motor2.set(speed);
	}
	public void driveRightSide(double speed)
	{
		motor3.set(speed);
		motor4.set(speed);
	}
	public void driveForward(double speed)
	{
		motor1.set(speed);
		motor2.set(speed);
		motor3.set(-speed);
		motor4.set(-speed);
	}
	public void driveLeft(double speed)
	{
		motor1.set(speed);
		motor2.set(speed);
		motor3.set(-speed);
		motor4.set(-speed);
	}
	/*
	public void driveLeftDegrees(double degrees)
	{
		while(Navx.getDegrees() != degrees)
		{
			driveLeft(.2);
		}
		driveForward(0);
	}
	*/
	public void driveRight(double speed)
	{
		motor1.set(-speed);
		motor2.set(-speed);
		motor3.set(speed);
		motor4.set(speed);
	}
	/*
	public void driveRightDegrees(double degrees)
	{
		while(Navx.getDegrees() != degrees)
		{
			driveLeft(.2);
		}
		driveForward(0);
	}
	*/
	public void resetEncoders()
	{
		motor3.setSelectedSensorPosition(0, 0, 10000);
		motor1.setSelectedSensorPosition(0, 0, 10000);
	}
	public boolean driveForInches(double inches, double speed) {
		return moveStraight(speed, (int)inches*TICKS_PER_INCH);
	}
	public boolean driveForFeet(double ticks, double speed) {
		return driveForInches(ticks * 12, speed);
	}
	public void driveForTicks(double d, double speed)
	{
		double speedM1 = speed;
		double speedM3 = speed;
		while(motor3.getSelectedSensorPosition(0) > (-d))
		{
		if(speedM1<= 0)
		{
			speedM1 = 0.1;
		}
		if(speedM3<= 0)
		{
			speedM3 = 0.1;
		}
		if(Math.abs(motor1.getSelectedSensorVelocity(0))>Math.abs(motor3.getSelectedSensorVelocity(0)))
		{
			speedM1 -= (Math.abs(motor1.getSelectedSensorVelocity(0))-Math.abs(motor3.getSelectedSensorVelocity(0)))/100;
		}
		else if(Math.abs(motor3.getSelectedSensorVelocity(0))>Math.abs(motor1.getSelectedSensorVelocity(0)))
		{
			speedM3 -= (Math.abs(motor3.getSelectedSensorVelocity(0))-Math.abs(motor1.getSelectedSensorVelocity(0)))/100;
		}
		driveLeftSide(speedM1);
		driveRightSide(-speedM3);
		SmartDashboard.putNumber("Encoder#3", motor3.getSelectedSensorPosition(0));
		SmartDashboard.putNumber("Encoder#1", motor1.getSelectedSensorPosition(0));
    	}
    	
		driveForward(0);
	}
	public boolean moveStraight(double speed, int ticks) {
		SmartDashboard.putNumber("errorrrrr", error);
		SmartDashboard.putNumber("speeed", speed);
		SmartDashboard.putNumber("encoder1", motor1.getSelectedSensorVelocity(0));
		SmartDashboard.putNumber("encoder2", motor3.getSelectedSensorVelocity(0));
		SmartDashboard.putNumber("valueright", motor3.getSelectedSensorPosition(0));
		SmartDashboard.putNumber("valueleft", motor1.getSelectedSensorPosition(0));
		
		if(motor3.getSelectedSensorPosition(0) < (ticks))
		{
		error = error + (motor1.getSelectedSensorVelocity(0) - motor3.getSelectedSensorVelocity(0))*0.0004; 
		
		if (Math.abs(speed) > 0.01) {
				//error =+ (motor1.getSelectedSensorVelocity(0) - motor3.getSelectedSensorVelocity(0))/150; 
			SmartDashboard.putNumber("speed12", speed-error);
			if (speed == 0) {
				speedzero++;
				SmartDashboard.putNumber("speedzero", speedzero);
			}
			SmartDashboard.putNumber("speed34", speed);

				motor1.set(speed-error);
				motor2.set(speed-error);

				motor3.set(speed);
				motor4.set(speed);

				

				return false;
		}
		return false;
		
		/*else if (speed < 0)
		{
				motor1.set(speed-error);
				motor3.set(speed);	
		}*/
		
		
		}
		else
		{
			motor1.set(0);
			motor2.set(0);
			motor3.set(0);
			motor4.set(0);
			return true;
			
		}
		
		}
	public boolean turnRight(double degrees)
	{
		if(Math.abs(navx.getAngle()) < Math.abs(degrees))
		{
			driveRight(.25);
			return false;
		}
		else
		{
			driveForward(0);
			
			return true;
		}
		
	}
	public boolean turnLeft(double degrees)
	{
		if(Math.abs(navx.getAngle()) < Math.abs(degrees))
		{
			driveLeft(.25);
			return false;
		}
		else
		{
			driveForward(0);
			
			return true;
		}
	
	}
	
	public void moveLiftUp(double speed)
	{
		if(!pin0.get())//upper limit
		{
			motor7.set(speed);
			motor8.set(speed);
			motor9.set(speed);
		}
	}
	public void moveLiftDown(double speed)
	{
		if(!pin1.get())//lower limit
		{
			motor7.set(-speed);
			motor8.set(-speed);
			motor9.set(-speed);
		}
	}
	public void moveArmUp(double speed)
	{
		motor10.set(speed);
	}
	public void moveArmDown(double speed)
	{
		motor10.set(speed);
	}
	public boolean moveArmPot(double speed, double angle)
	{
		//SmartDashboard.putNumber("pottT", pot.get());
		if(pot.get() > angle)
		{
			motor10.set(-speed);
			return false;
		}
		else if(pot.get() < angle)
		{
			motor10.set(speed);
			return false;
		}
		else
		{
			motor10.set(0);
			return true;
		}
	}
	public void pushCube(double speed)
	{
		motor5.set(speed);
		motor6.set(speed);
	}
	public void suckCube(double speed)
	{
		motor5.set(-speed);
		motor6.set(-speed);
	}
	
}
