package testRobotEncoders;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
//import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Timer;
//import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import org.usfirst.frc.team5410.robot.RotateMotor;

//import java.util.concurrent.TimeUnit;

import com.ctre.phoenix.motorcontrol.*;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class differenceBetweenRightAndLeftMotors extends IterativeRobot {

	final String defaultAuto = "Testing (Default) Auto"; // The name of the default auto mode
	final String customAuto = "Game Auto"; // Main auto mode
	final String blockAuto = "Block Auto";
	final String basicAuto = "Basic Auto";
	String autoSelected;
	SendableChooser<String> chooser = new SendableChooser<>();
	
	//CANTalon motor1 = new CANTalon(0);
	WPI_TalonSRX motor1 = new WPI_TalonSRX(0);
	WPI_TalonSRX motor2 = new WPI_TalonSRX(1);
	WPI_TalonSRX motor3 = new WPI_TalonSRX(2);
	WPI_TalonSRX motor4 = new WPI_TalonSRX(3);
	Joystick joystick1 = new Joystick(0);
	
	public static Timer timer = new Timer(); // Keep track of time

	//Encoder enc = new Encoder(0, 1, false, Encoder.EncodingType.k4X); // Not needed; WPI_TalonSRX has everything we need
	
	
	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() {
		chooser.addDefault("Default Auto", defaultAuto);
		chooser.addObject("Game Auto", customAuto);
		chooser.addObject("Block Auto", blockAuto);
		chooser.addObject("Block Auto", basicAuto);
		SmartDashboard.putData("Auto choices", chooser);
		motor4.configSelectedFeedbackSensor(com.ctre.phoenix.motorcontrol.FeedbackDevice.QuadEncoder, 0, 0);
		motor4.setSensorPhase(false);
	}

	/**
	 * This autonomous (along with the chooser code above) shows how to select
	 * between different autonomous modes using the dashboard. The sendable
	 * chooser code works with the Java SmartDashboard. If you prefer the
	 * LabVIEW Dashboard, remove all of the chooser code and uncomment the
	 * getString line to get the auto name from the text box below the Gyro
	 *
	 * You can add additional auto modes by adding additional comparisons to the
	 * switch structure below with additional strings. If using the
	 * SendableChooser make sure to add them to the chooser code above as well.
	 */
	@Override
	public void autonomousInit() { // Called as soon as Auto mode starts

		autoSelected = chooser.getSelected();
		// autoSelected = SmartDashboard.getString("Auto Selector",
		// defaultAuto);
		System.out.println("Auto selected: " + autoSelected);
		
		timer.reset();
		timer.start();
	}

	/**
	 * This function is called periodically during autonomous (~ every 20 miliseconds)
	 */
	@Override
	public void autonomousPeriodic() {
		
		//motor1.getSelectedSensorPosition(0);
		
		if (timer.get() >= 15) {
			timer.stop();
			timer.reset();
			return;
		}
		
		switch (autoSelected) {
		case customAuto:
			
			crossLine();


			break;
		
		case blockAuto:

			
			break;
		case defaultAuto:
		
		default:
			
 

			break;
		}
	}
	//SerialPort port = new SerialPort(0, null);
	
	private void crossLine() {
		motor4.set(0.5);
		while(timer.get() < 5) {
			
		}
		return;
	}
	private void 
}
