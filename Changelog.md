# Overall Programming Changelog

* 1/11
  * found [GradleRIO](https://github.com/Open-RIO/GradleRIO), which could allow people to build code on non-Windows computers without WPILIB/CAN Talon SRX files
* 1/12
  * imaged roboRIO for this season
  * confirmed joysticks are operational by adding code to print positions of the sticks while code is running
* 1/14
  * implemented GradleRIO so now all commits to the Github are tested automatically
  * also people on non-Windows OSes can write code now which is pretty neat
* 1/15
  * started repo anew; now project is just in "powerup-project"
  * made command to rotate robot motor
  * made two autonomous modes and added timer to keep them in check
