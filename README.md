[![Build Status](https://travis-ci.org/NBPSEaglebotics/powerup.svg?branch=master)](https://travis-ci.org/NBPSEaglebotics/powerup)
[![Maintainability](https://api.codeclimate.com/v1/badges/62a941d987326d9dcfe0/maintainability)](https://codeclimate.com/github/NBPSEaglebotics/powerup/maintainability)

*Note: if you don't actually change code, put "[skip ci]" in your commit message (without quotes)*

# Changes and Progress

See [Changelog.md](Changelog.md).

# How To Install

See [this guide](https://medium.com/@eaglebotics/how-to-install-an-frc-eclipse-project-with-gradlerio-3d859b3fe7a7) to get started.

### Eclipse Development Plugins

Install the development plugins from [here](https://wpilib.screenstepslive.com/s/currentCS/m/getting_started/l/599679-installing-eclipse-c-java#installing-the-development-plugins-option-1-online-install).

Now that you've installed, you can clone this repository and open it with Eclipse (refer to the [gitguide](#how-to). 

## Control System

This guide is adapted from [here](https://wpilib.screenstepslive.com/s/4485).

*Installing the FRC Update Suite requires a Windows computer.*

### Update Suite (includes Driver Station)

Follow [this guide](https://wpilib.screenstepslive.com/s/currentCS/m/getting_started/l/599670-installing-the-frc-update-suite-all-languages) as it pertains to Java teams.
